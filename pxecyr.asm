bits 16
org 0

entry:
	; canonicalize addresses
	jmp 0x7c0:main

main:
	pushad

	mov ax,0x7c0
	mov es,ax

	; load font
	mov bx,1000h    ; 16 bytes per character
	mov cx,256      ; load whole character set
	xor dx,dx       ; start with charcode 0
	mov bp,fontdata
	mov ax,1110h
	int 10h

	popad

	; report success
	xor ax,ax
	retf

fontdata:
	%include "fontdata.inc"
