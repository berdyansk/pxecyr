CC=nasm
CFLAGS=-f bin

all: pxecyr.com

clean:
	rm -f pxecyr.com

pxecyr.com:
	$(CC) $(CFLAGS) pxecyr.asm -o pxecyr.com
